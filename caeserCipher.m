function cipher = caeserCipher(clearText, n)
  assert(abs(n) < 26, "n is beyond shift limit");
  cipher = "";
  for i = 1:length(clearText)
    cipher = cstrcat(cipher, char(shiftLetter(toascii(clearText(i)), n)));
  endfor
endfunction

function d = shiftLetter(c, n)
  if(c == 32)
    d = 32;
  else
    x = 0;
    if((c == 65 || c == 97) && n < 0)
      x = c + 26;
    elseif((c == 90 || c == 122) && n > 0)
      x = c - 26;
    else
      x = c;
    endif
    
    if(n == 0)
      d = c;
    elseif(n < 0)
      d = shiftLetter(x - 1, n + 1);
    else
      d = shiftLetter(x + 1, n - 1);
    endif
  endif
  
endfunction